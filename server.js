﻿require('dotenv').config();
var express = require('express');
var app = express();
var path = require('path');
var port = process.env.PORT;
var distPath = path.join(__dirname, 'dist');


// Run the app by serving the static files
// in the dist directory
app.use(express.static(distPath));

app.get('*', function (request, response) {
  response.sendFile(path.resolve(distPath, 'index.html'));
});

app.listen(port, function () {
  console.log('Listening on port ' + port); //eslint-disable-line angular/log,no-console
});
