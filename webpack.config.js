'use strict';
const validate = require('webpack-validator');
const buildDefaultVariables = require('./config/defaultOptions');

const moduleConfig = require('./config/module');
const buildEntryConfig = require('./config/entry');
const buildOutputConfig = require('./config/output');
const buildPluginsConfig = require('./config/plugins');
const buildDevtoolConfig = require('./config/devtool');
const buildPostCSSConfig = require('./config/postcss');
const buildDevServerConfig = require('./config/devServer');
const esLintConfig = require('./config/eslint');

const options = buildDefaultVariables(__dirname);

const config = {
  entry: buildEntryConfig(options),
  output: buildOutputConfig(options),
  module: moduleConfig,
  eslint: esLintConfig,
  plugins: buildPluginsConfig(options),
  debug: !options.isProd,
  devtool: buildDevtoolConfig(options),
  devServer: buildDevServerConfig(options),
  postcss: buildPostCSSConfig()
};

module.exports = validate(config, {quiet: true});
