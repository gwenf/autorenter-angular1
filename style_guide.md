# AutoRenter Style Guide - Angular 1

This style guide, which supplements the *AutoRenter Style Guide - General* document, is based mostly on the [John Papa Style Guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md).

This style guide is meant to compliment Papa's style guide with information specific to our project.

## Naming

### Prefix

Custom Components and Filters should be prefixed with "fa".
