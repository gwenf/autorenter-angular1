const path = require('path');
const args = require('yargs').argv;
const webpack = require('webpack');
const parse = require('url-parse');
require('dotenv').config();

const unitTestEntry = 'test/unit/helper.js';

// run multiple times in watch mode
const singleRun = !args.watch;
// use phantomjs in watch mode
const browser = args.debug ? ['Chrome'] : ['PhantomJS'];
const files = [{pattern: unitTestEntry, watched: false, served: true, included: true}];
const include = [path.resolve('./src')];
const isProd = process.env.__PROD__ == 'true' || process.env.isProd || args.prod;
const apiUrl = process.env.__APIURL__;

const webpackPlugins = [
  new webpack.DefinePlugin({
    __PROD__: JSON.stringify(isProd),
    __APIURL__: JSON.stringify(apiUrl)
  })
];

const preLoaders = [
  // Process all test code
  {test: /\.spec\.js$/, loaders: [], include: include},
  // Process all non-test code with Isparta
  {test: /\.js$/, loaders: args.debug ? [] : ['istanbul-instrumenter-loader'], include: include, exclude: /\.spec\.js$/}
];

const loaders = [
  {test: /\.(png|jpg)$/, loader: 'null'},
  {test: /\.(html)$/, loader: 'html'},
  {test: /sinon(\\|\/)pkg(\\|\/)sinon\.js/, loader: 'imports?define=>false,require=>false'}
];

const processors = {};
processors[unitTestEntry] = ['webpack', 'sourcemap'];

let reporters = args.ci ? [
  'mocha', 'coverage'
] : [
  'mocha'
];

reporters = args.debug ? reporters.splice('coverage', 1) : reporters;

const coverageReporters = args.watch ? [
  {type: 'text-summary'}
] : [
  {type: 'lcov', subdir: '.'},
  {type: 'text-summary'}
];

module.exports = function karmaConfig(config) {
  config.set({
    basePath: '.',
    frameworks: ['mocha'],
    exclude: [],
    files: files,
    webpack: {
      devtool: 'inline-source-map',
      plugins: webpackPlugins,
      module: {
        preLoaders: preLoaders,
        loaders: loaders,
        noParse: [
          /\/sinon\.js/
        ]
      },
      cache: true,
      resolve: {
        alias: {
          sinon: 'sinon/pkg/sinon'
        }
      }
    },
    webpackMiddleware: {
      stats: {
        chunkModules: false,
        colors: true
      },
      noInfo : true
    },
    preprocessors: processors,
    reporters: reporters,
    coverageReporter: {
      dir: './coverage',
      reporters: coverageReporters,
      instrumenterOptions: {
        istanbul: { noCompact: true }
      }
    },
    reportSlowerThan: 500,
    singleRun: singleRun,
    browsers: [browser],
    concurrency: Infinity
  });
};
