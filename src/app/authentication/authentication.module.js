'use strict';

var angular = require('angular');
var localStorage = require('../local-storage/local-storage.module');
var uiRouter = require('angular-ui-router');

module.exports = angular.module('fa.authentication', [
  localStorage.name,
  uiRouter
]);
