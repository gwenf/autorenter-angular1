'use strict';

var authentication = require('./authentication.module');

require('./authentication-service.factory');

module.exports = authentication;
