'use strict';

var authentication = require('./authentication.module');

function authenticationService(localStorageService, $injector) {
  function goToState(state) {
    // We have to use the injector to get around a circular dependency.
    var stateService = $injector.get('$state');
    stateService.go(state);
  }

  function logIn(credentials) {
    localStorageService.setCredentials(credentials);
    goToState('main');
  }

  function logOut() {
    localStorageService.clearCredentials();
    goToState('login');
  }

  function getUserToken() {
    return localStorageService.getUserToken();
  }

  function isUserAuthenticated() {
    return !!getUserToken();
  }

  function getCredentials() {
    return localStorageService.getCredentials();
  }

  return {
    logIn: logIn,
    logOut: logOut,
    getUserToken: getUserToken,
    isUserAuthenticated: isUserAuthenticated,
    getCredentials: getCredentials
  };
}

authenticationService.$inject = ['faLocalStorageService', '$injector'];

authentication.factory('faAuthenticationService', authenticationService);
