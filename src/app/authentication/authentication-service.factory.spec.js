'use strict';

require('./authentication');

describe('fa.authentication.faAuthenticationService > ', function describeImpl() {
  var authenticationService;
  var localStorageService;
  var injectorStub;
  var fakeStateService;
  var goSpy;

  beforeEach(angular.mock.module('fa.authentication'));

  beforeEach(inject(function injectImpl(_faAuthenticationService_, _faLocalStorageService_, _$injector_) {
    authenticationService = _faAuthenticationService_;
    localStorageService = _faLocalStorageService_;

    fakeStateService = {
      go: function go() {}
    };
    goSpy = sinon.spy(fakeStateService, 'go');
    injectorStub = sinon.stub(_$injector_, 'get', function get(dependencyName) {
      return dependencyName === '$state' ? fakeStateService : null;
    });
  }));

  describe('login', function logInTests() {
    var credentials;
    beforeEach(function beforeEachImpl() {
      credentials = {
        token: 'token',
        username: 'john',
        userId: 'abc123'
      };
    });

    it('should set credentials', function testImpl() {
      var setCredentialsSpy = sinon.spy(localStorageService, 'setCredentials');
      authenticationService.logIn(credentials);
      setCredentialsSpy.args.should.deep.equal([[credentials]]);      
    });

    it('should navigate to the main screen', function testImpl() {
      authenticationService.logIn(credentials);
      goSpy.args.should.deep.equal([['main']]);
    });
  });

  describe('logOut', function logOutTests() {
    it('should clear the credentials', function testImpl() {
      var clearCredentialsSpy = sinon.spy(localStorageService, 'clearCredentials');
      authenticationService.logOut();
      expect(clearCredentialsSpy.called).to.be.true;
    });

    it('should navigate to the login screen', function testImpl() {
      authenticationService.logOut();
      goSpy.args.should.deep.equal([['login']]);
    });
  });

  describe('getUserToken', function getUserTokenTests() {
    it('should delegate to the local storage service', function testImpl() {
      var userToken = 'token';
      sinon.stub(localStorageService, 'getUserToken',
        function getUserToken() {
          return userToken;
        });
      var actualToken = authenticationService.getUserToken();
      expect(actualToken).to.equal(userToken);
    });
  });

  describe('isUserAuthenticated', function isUserAuthenticatedTests() {
    it('should return true', function testImpl() {
      var userToken = 'token';
      sinon.stub(localStorageService, 'getUserToken',
        function getUserToken() {
          return userToken;
        });
      authenticationService.isUserAuthenticated().should.be.true;
    });

    it('should return false', function testImpl() {
      var userToken = false;
      sinon.stub(localStorageService, 'getUserToken',
        function getUserToken() {
          return userToken;
        });
      authenticationService.isUserAuthenticated().should.be.false;
    });
  });

  describe('getCredentials', function getCredentialsTests() {
    it('should return credentials', function testImpl() {
      var credentials = {foo: 'bar'};
      sinon.stub(localStorageService, 'getCredentials',
        function getCredentials() {
          return credentials;
        });
      authenticationService.getCredentials().should.deep.equal(credentials);
    });
  });

  afterEach(function afterEach() {
    injectorStub.reset();
  });

});
