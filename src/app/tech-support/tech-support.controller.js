'use strict';

var techSupport = require('./tech-support.module');

function TechSupportController($log, notificationService, rootDataService, raiseErrorDataService) {
  var vm = this;

  vm.showError = function showError() {
    notificationService.notifyError({
      title: 'The error title',
      userMessage: 'Oops, an error!',
      technicalMessage: 'technical error message'
    });
  };

  vm.showWarning = function showWarning() {
    notificationService.notifyWarning({
      userMessage: 'A warning',
      technicalMessage: 'technical warning message'
    });
  };

  vm.showInfo = function showInfo() {
    notificationService.notifyInfo({
      userMessage: 'An informational message',
      technicalMessage: 'technical info message'
    });
  };

  vm.showSuccess = function showSuccess() {
    notificationService.notifySuccess({
      userMessage: 'Hooray, it worked!',
      technicalMessage: 'technical success message'
    });
  };

  vm.throwException = function throwException() {
    throw new Error('the test exception');
  };

  vm.checkHttpErrorHandlerError = function checkHttpErrorHandler() {
    raiseErrorDataService.getRaiseError()
    .then(function setResult(response) {
      $log.debug('success - response = ' + response);
    }).catch(function errorCallback(response) {
      $log.debug('unexpected error - response = ' + response);
    });
  };

  vm.getApiInfo = function getApiInfo() {
    rootDataService.getRoot()
      .then(function assignData(response) {
        vm.apiResponse = response;
      });
  };

  vm.getApiInfo();
}

TechSupportController.$inject = [
  '$log',
  'faNotificationService',
  'faRootDataService',
  'faRaiseErrorDataService'
];

techSupport.controller('FaTechSupportController', TechSupportController);
