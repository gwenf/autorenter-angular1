'use strict';

require('./tech-support.controller');

describe('fa.techSupport.TechSupportController > ', function describeImpl() {
  var controller;
  var $q;
  var $scope;
  var $log;
  var notificationService;
  var raiseErrorDataService;

  var getRootStub;

  var expectedGetRootResponse = 'foo';

  beforeEach(angular.mock.module('fa.techSupport'));

  beforeEach(inject(function injectImpl(_$q_,
                                        _$rootScope_,
                                        _$log_,
                                        _faNotificationService_,
                                        _faRootDataService_,
                                        _faRaiseErrorDataService_) {

    $q = _$q_;
    $scope = _$rootScope_.$new();
    $log = _$log_;
    notificationService = _faNotificationService_;
    raiseErrorDataService = _faRaiseErrorDataService_;

    getRootStub = sinon.stub(_faRootDataService_, 'getRoot', function getRoot() {
      return $q.resolve(expectedGetRootResponse);
    });

    angular.mock.inject([
      '$controller',
      function assignController($controller) {
        controller = $controller('FaTechSupportController', {
          '$log': $log,
          'faNotificationService': notificationService,
          'faRootDataService': _faRootDataService_,
          'faRaiseErrorDataService': raiseErrorDataService
        });
      }
    ]);

    // Run the controller initialization function...
    $scope.$apply();
  }));

  afterEach(function afterEach() {
    getRootStub.reset();
  });

  describe('getApiInfo', function getApiInfoTest() {
    it('should be automatically invoked to initialize apiResponse', function testImpl() {
      expect(controller.apiResponse).to.equal(expectedGetRootResponse);
    });
  });

  describe('showError', function showErrorTest() {
    it('should invoke showError method', function testImpl() {
      var notifyErrorSpy = sinon.spy(notificationService, 'notifyError');
      controller.showError();
      notifyErrorSpy.calledWith({
        title: 'The error title',
        userMessage: 'Oops, an error!',
        technicalMessage: 'technical error message'
      }).should.be.true;
    });
  });

  describe('showWarning', function showWarningTest() {
    it('should invoke showWarning method', function testImpl() {
      var notifyWarningSpy = sinon.spy(notificationService, 'notifyWarning');
      controller.showWarning();
      notifyWarningSpy.calledWith({
        userMessage: 'A warning',
        technicalMessage: 'technical warning message'
      }).should.be.true;
    });
  });

  describe('showInfo', function showInfoTest() {
    it('should invoke showInfo method', function testImpl() {
      var notifyInfoSpy = sinon.spy(notificationService, 'notifyInfo');
      controller.showInfo();
      notifyInfoSpy.calledWith({
        userMessage: 'An informational message',
        technicalMessage: 'technical info message'
      }).should.be.true;
    });
  });

  describe('showSuccess', function showSuccessTest() {
    it('should invoke showSuccess method', function testImpl() {
      var notifySuccessSpy = sinon.spy(notificationService, 'notifySuccess');
      controller.showSuccess();
      notifySuccessSpy.calledWith({
        userMessage: 'Hooray, it worked!',
        technicalMessage: 'technical success message'
      }).should.be.true;
    });
  });

  describe('throwException', function throwExceptionTest() {
    it('should throw an exception', function testImpl() {
      return expect(function () {
        controller.throwException();
      }).to.throw(Error, 'the test exception');
    });
  });

  describe('checkHttpErrorHandlerError', function checkHttpErrorHandlerErrorTest() {
    var logStub;
    var getRaiseErrorStub;

    beforeEach(function beforeEach() {
      logStub = sinon.stub($log, 'debug');
    });

    it('should log success', function testImpl() {
      var expectedMessage = 'success - response = foobar';
      getRaiseErrorStub = sinon.stub(raiseErrorDataService, 'getRaiseError',
        function getRaiseError() {
          return $q.resolve('foobar');
        });

      controller.checkHttpErrorHandlerError();
      $scope.$apply();

      expect(logStub.calledWith(expectedMessage)).to.be.true;
    });

    it('should log unexpected error', function testImpl() {
      var expectedMessage = 'unexpected error - response = foobar';
      getRaiseErrorStub = sinon.stub(raiseErrorDataService, 'getRaiseError',
        function getRaiseError() {
          return $q.reject('foobar');
        });

      controller.checkHttpErrorHandlerError();
      $scope.$apply();

      expect(logStub.calledWith(expectedMessage)).to.be.true;
    });

    afterEach(function afterEach() {
      logStub.reset();
      getRaiseErrorStub.reset();
    });
  });
});
