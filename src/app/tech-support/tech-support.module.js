'use strict';

var angular = require('angular');
var config = require('../config/config');
var notifications = require('../notifications/notifications');
var uiRouter = require('angular-ui-router');
var dataAccess = require('../data-access/data-access');

module.exports = angular.module('fa.techSupport', [
  config.name,
  notifications.name,
  uiRouter,
  dataAccess.name
]);
