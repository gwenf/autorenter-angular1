'use strict';

var app = require('./app.module');

function runBlock($rootScope, $state, authenticationService) {
  var deregisterRouteChangeStart = $rootScope.$on('$locationChangeStart', function () {
    if (!authenticationService.isUserAuthenticated()) {
      $state.go('login');
    }
  });

  $rootScope.$on('$destroy', deregisterRouteChangeStart);
}

runBlock.$inject = [
  '$rootScope',
  '$state',
  'faAuthenticationService'
];

app.run(runBlock);
