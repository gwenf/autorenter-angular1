'use strict';

var angular = require('angular');

require('../assets/css/app.css');
require('bootstrap/dist/css/bootstrap.min.css');
require('angular-ui-bootstrap/dist/ui-bootstrap-csp.css');
require('angularjs-toaster/toaster.min.css');
require('angular-ui-grid/ui-grid.css');
require('angular-loading-bar/build/loading-bar.css');

require('angular-animate');
require('angular-ui-router');
require('angular-breadcrumb');
require('angular-loading-bar');

// components
var config = require('./config/config');
var logging = require('./logging/logging');
var errorHandling = require('./error-handling/error-handling');
var notifications = require('./notifications/notifications');
var confirmations = require('./confirmations/confirmations');
var techSupport = require('./tech-support/tech-support');
var login = require('./login/login');
var main = require('./main/main');
var admin = require('./admin/admin');
var fleet = require('./fleet/fleet');
var navBar = require('./nav-bar/nav-bar');
var authentication = require('./authentication/authentication');
var localStorage = require('./local-storage/local-storage');

module.exports = angular.module('app', [
  'ngAnimate',
  'ui.router',
  'ncy-angular-breadcrumb',
  'angular-loading-bar',
  config.name,
  logging.name,
  errorHandling.name,
  notifications.name,
  confirmations.name,
  techSupport.name,
  login.name,
  main.name,
  admin.name,
  fleet.name,
  navBar.name,
  authentication.name,
  localStorage.name
]);
