'use strict';

var main = require('./main.module');

function MainController($rootScope, $log, authenticationService) {
  var vm = this;

  var username = authenticationService.getCredentials().username;
  var isUserAuthenticated = authenticationService.isUserAuthenticated();

  vm.user = {
    username: username,
    isUserAuthenticated: isUserAuthenticated
  }; 

  vm.initialize = function initialize() {
    var deregisterLogin = $rootScope.$on('login', function() {
      var username = authenticationService.getCredentials().username;
      vm.user.username = username;
      vm.user.isUserAuthenticated = true;
      $log.debug('username', username);
    });

    $rootScope.$on('$destroy', deregisterLogin);

    var deregisterLogout = $rootScope.$on('logout', function() {
      vm.user.username = '';
      vm.user.isUserAuthenticated = false;
      $log.debug('logout');
    });

    $rootScope.$on('$destroy', deregisterLogout);
  };

  vm.initialize();
}

MainController.$inject = [
  '$rootScope',
  '$log',
  'faAuthenticationService'
];

main.controller('MainController', MainController);
