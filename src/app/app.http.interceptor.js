'use strict';
function httpInterceptor(authenticationService) {
  return {
    request: requestInterceptor,
    responseError: responseErrorInterceptor
  };

  function requestInterceptor(config) {
    config.headers['Authorization'] = authenticationService.getUserToken();
    return config;
  }

  function responseErrorInterceptor(response) {
    if (response.status === 401) {
      authenticationService.logOut();
    }
    throw response;
  }
}

httpInterceptor.$inject = ['faAuthenticationService'];

module.exports = angular.module('app')
.factory('faHttpInterceptor', httpInterceptor)
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('faHttpInterceptor');
  });
