'use strict';

require('./app.module');
require('./app.config');
require('./app.route.config');
require('./app.run');
require('./app.http.interceptor');
