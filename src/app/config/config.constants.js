'use strict';

var angular = require('angular');
var configModule = require('./config.module');

var apiUrl = __APIURL__;

var configData = {
  generalConfig: {
    apiUrlRoot: apiUrl,
    sourcePathRoot: 'app/',    
  }
};

angular.forEach(configData, function configInit(key, value) {
  configModule.constant(value, key);
});
