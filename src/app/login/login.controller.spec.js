'use strict';

require('./login.controller');

describe('fa.login.LoginController > ', function describeImpl() {
  var $q;
  var $rootScope;
  var authenticationService;
  var loginDataService;
  var controller;
  var loginDataServiceStub;

  var credentials = {
    token: 'token',
    username: 'john',
    userId: 'abc123'
  };

  beforeEach(angular.mock.module('fa.login'));
  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));
  beforeEach(angular.mock.module('fa.authentication'));

  beforeEach(inject(function injectImpl(
    _$q_,
    _$rootScope_,
    _faAuthenticationService_, 
    _faLoginDataService_) {
    $q = _$q_;
    $rootScope = _$rootScope_;
    authenticationService = _faAuthenticationService_;
    loginDataService = _faLoginDataService_;

    angular.mock.inject([
      '$controller',
      function assignController($controller) {
        controller = $controller('FaLoginController', {
          'faLoginDataService': loginDataService,
          'faAuthenticationService': authenticationService,
          '$rootScope': $rootScope
        });
      }
    ]);
  }));

  describe('login - on failure', function initializeImpl() {
    beforeEach(function beforeEachImpl() {
      loginDataServiceStub = sinon.stub(loginDataService, 'login', function login() {
        var deferred = $q.defer();
        deferred.resolve({status: 401, data: credentials});
        return deferred.promise;
      });
    });

    it('should set loginFailed to true', function testImpl() {
      controller.login();
      $rootScope.$apply();

      controller.loginFailed.should.be.true;
    });

    afterEach(function afterEachImpl() {
      loginDataServiceStub.reset();
    });
  });

  describe('login - on success', function initializeImpl() {
    var authenticationServiceStub;
    beforeEach(function beforeEachImpl() {
      loginDataServiceStub = sinon.stub(loginDataService, 'login', function login() {
        var deferred = $q.defer();
        deferred.resolve({status: 200, data: credentials});
        return deferred.promise;
      });

      authenticationServiceStub = sinon.stub(authenticationService, 'logIn', function logIn() {});
    });

    it('should set loginFailed to false', function testImpl() {
      controller.login();
      $rootScope.$apply();

      controller.loginFailed.should.be.false;
    });

    it('should forward credentials to the authentication service', function testImpl() {
      controller.login();
      $rootScope.$apply();

      authenticationServiceStub.args.should.deep.equal([[credentials]]);
    });

    it('should emit login event', function testImpl() {
      var emitSpy = sinon.spy($rootScope, '$emit');

      controller.login();
      $rootScope.$apply();

      emitSpy.args.should.deep.equal([['login']]);
    });

    afterEach(function afterEachImpl() {
      loginDataServiceStub.reset();
      authenticationServiceStub.reset();
    });
  });
});
