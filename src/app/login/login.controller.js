'use strict';

var login = require('./login.module');

function LoginController(loginDataService, authenticationService, $rootScope) {
  var vm = this;

  vm.login = function login() {
    loginDataService.login(vm.username, vm.password)
      .then(function (response) {
        if (response.status == 200) {
          vm.loginFailed = false;
          authenticationService.logIn(response.data);
          $rootScope.$emit('login');
        } else {
          vm.loginFailed = true;
        }
      });
  };
}

LoginController.$inject = [
  'faLoginDataService',
  'faAuthenticationService',
  '$rootScope'
];

login.controller('FaLoginController', LoginController);
