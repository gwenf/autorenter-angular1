'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var localStorageModule = require('../local-storage/local-storage.module');

module.exports = angular.module('fa.login', [
  uiRouter,
  localStorageModule.name
]);
