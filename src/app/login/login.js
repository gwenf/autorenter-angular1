'use strict';

var login = require('./login.module');

require('./login.route.config');
require('./login.controller');

module.exports = login;
