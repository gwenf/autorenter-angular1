'use strict';

var login = require('./login.module');
var loginTemplate = require('./login.html');

function loginRouteConfig($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      views: {
        '@': {
          template: loginTemplate,
          controller: 'FaLoginController',
          controllerAs: 'vm'
        }
      }
    });
}

loginRouteConfig.$inject = ['$stateProvider'];

login.config(loginRouteConfig);
