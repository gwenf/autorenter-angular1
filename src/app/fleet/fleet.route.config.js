'use strict';

var fleet = require('./fleet.module');
var fleetHeaderTemplate = require('./fleet.header.html');
var fleetLocationsTemplate = require('./locations/fleet-locations.html');
var fleetLocationDetailsTemplate = require('./locations/detail/fleet-location-details.html');
var fleetLocationVehiclesTemplate = require('./vehicles/fleet-location-vehicles.html');
var fleetLocationVehicleDetailsTemplate = require('./vehicles/detail/fleet-location-vehicle-details.html');
var fleetReportsTemplate = require('./reports/fleet-reports.html');

function fleetRouteConfig($stateProvider) {
  $stateProvider
    .state('fleet', {
      abstract: true,
      views: {
        'moduleHeader@': {
          template: fleetHeaderTemplate
        }
      }
    })
    .state('fleet.locations', {
      abstract: true
    })
    .state('fleet.locations.list', {
      url: '/fleet/locations',
      views: {
        '@': {
          template: fleetLocationsTemplate,
          controller: 'FaFleetLocationsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: 'Locations'
      }
    })
    .state('fleet.locations.add', {
      url: '/add',
      views: {
        '@': {
          template: fleetLocationDetailsTemplate,
          controller: 'FaFleetLocationDetailsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: 'Add'
      },
      parent: 'fleet.locations.list'
    })
    .state('fleet.locations.view', {
      url: '/{locationId}/view',
      views: {
        '@': {
          template: fleetLocationDetailsTemplate,
          controller: 'FaFleetLocationDetailsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: '{{::vm.location.siteId}}'
      },
      parent: 'fleet.locations.list'
    })
    .state('fleet.locations.edit', {
      url: '/{locationId}/edit',
      views: {
        '@': {
          template: fleetLocationDetailsTemplate,
          controller: 'FaFleetLocationDetailsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: '{{::vm.location.siteId}}'
      },
      parent: 'fleet.locations.list'
    })
    .state('fleet.locations.vehicles', {
      url: '/{locationId}/vehicles',
      views: {
        '@': {
          template: fleetLocationVehiclesTemplate,
          controller: 'FaFleetLocationVehiclesController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: 'Vehicles',
        parent: 'fleet.locations.view'
      },
      parent: 'fleet.locations.list'
    })
    .state('fleet.locations.vehicles.view', {
      url: '/{vehicleId}/view',
      views: {
        '@': {
          template: fleetLocationVehicleDetailsTemplate,
          controller: 'FaFleetLocationVehicleDetailsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: '{{::vm.vehicle.vin}}'
      },
      parent: 'fleet.locations.vehicles'
    })
    .state('fleet.locations.vehicles.edit', {
      url: '/{vehicleId}/edit',
      views: {
        '@': {
          template: fleetLocationVehicleDetailsTemplate,
          controller: 'FaFleetLocationVehicleDetailsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: '{{::vm.vehicle.vin}}'
      },
      parent: 'fleet.locations.vehicles'
    })
    .state('fleet.locations.vehicles.add', {
      url: '/add',
      views: {
        '@': {
          template: fleetLocationVehicleDetailsTemplate,
          controller: 'FaFleetLocationVehicleDetailsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: 'Add'
      },
      parent: 'fleet.locations.vehicles'
    })
    .state('fleet.reports', {
      abstract: true
    })
    .state('fleet.reports.list', {
      url: '/fleet/reports',
      views: {
        '@': {
          template: fleetReportsTemplate,
          controller: 'FaFleetReportsController',
          controllerAs: 'vm'
        }
      },
      ncyBreadcrumb: {
        label: 'Reports'
      }
    });
}

fleetRouteConfig.$inject = ['$stateProvider'];

fleet.config(fleetRouteConfig);
