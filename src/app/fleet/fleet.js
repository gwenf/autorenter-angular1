'use strict';

var fleet = require('./fleet.module');

require('./fleet.route.config');

require('./locations/fleet-locations.controller');
require('./vehicles/fleet-location-vehicles.controller');

require('./locations/detail/fleet-location-add-strategy.factory');
require('./vehicles/detail/fleet-location-vehicle-add-strategy.factory');

require('./locations/detail/fleet-location-strategy-factory.factory');
require('./vehicles/detail/fleet-location-vehicle-strategy.factory');

require('./locations/detail/fleet-location-edit-strategy.factory');
require('./vehicles/detail/fleet-location-vehicle-edit-strategy.factory');

require('./locations/detail/fleet-location-details.controller');
require('./vehicles/detail/fleet-location-vehicle-details.controller');

require('./locations/detail/fleet-location-details-mode-service.factory');
require('./vehicles/detail/fleet-location-vehicle-details-mode-service.factory');

require('./locations/detail/fleet-location-view-strategy.factory');
require('./vehicles/detail/fleet-location-vehicle-view-strategy.factory');

require('./locations/detail/fleet-location-initialization.factory');
require('./vehicles/detail/fleet-location-vehicle-initialization.factory');

require('./vehicles/detail/fleet-vehicle-property-synchronizer.factory');
require('./vehicles/detail/vehicle-image-service.factory');

require('./reports/fleet-reports.controller');

module.exports = fleet;
