var angular = require('angular');
var sinon = require('sinon');
require('angular-mocks');
require('sinon-chai');

describe('fa.fleet.vehicleImageService > ', function describeImpl() {
  var $rootScope;
  var vehicleImageService;

  beforeEach(angular.mock.module('fa.fleet'));
  beforeEach(inject(function injectImpl(_$rootScope_,
                                        _faVehicleImageService_) {
    $rootScope = _$rootScope_;
    vehicleImageService = _faVehicleImageService_;
  }));

  describe('getImageData', function getImageDataTests() {
    var readDataStub;
    var expectedImageData = 'foo';
    var event = {
      target: {
        result: expectedImageData
      }
    };
    var imageFile = {};

    beforeEach(function beforeEach() {
      readDataStub = sinon.stub(FileReader.prototype, 'readAsDataURL', function readAsDataURL(file) {
        this.onload(file === imageFile ? event : null);
      });
    });

    it('returns the image file data', function testImpl() {
      var imageData = {};
      vehicleImageService.getImageData(imageFile)
        .then(function (data) {
          imageData = data;
        });
      $rootScope.$apply();
      expect(imageData).to.equal(expectedImageData);
    });

    afterEach(function afterEach() {
      readDataStub.reset();
    });
  });

});
