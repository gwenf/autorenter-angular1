'use strict';

var fleet = require('./../../fleet.module');

function FleetLocationVehicleDetailsController($scope,
                                               $state,
                                               fleetLocationVehicleStrategyFactory,
                                               fleetLocationVehicleDetailsModeService,
                                               fleetVehiclePropertySynchronizer,
                                               vehicleImageService) {
  var vm = this;

  vm.location = {};
  vm.vehicle = {};
  vm.makes = {};
  vm.models = {};
  vm.years = {};
  vm.colors = {};
  vm.selectedMake = {};
  vm.selectedModel = {};
  vm.imageFileErrorMessage = '';

  var implementationStrategy;

  vm.initialize = function initialize() {
    implementationStrategy = fleetLocationVehicleStrategyFactory.getStrategy();
    implementationStrategy.getInitializationData($state.params.locationId, $state.params.vehicleId)
      .then(function init(initializationData) {
        vm.location = initializationData.location;
        vm.vehicle = initializationData.vehicle || {};
        vm.makes = initializationData.makes;
        vm.selectedMake = initializationData.selectedMake;
        vm.selectedModel = initializationData.selectedModel;
        fleetVehiclePropertySynchronizer.initialize(initializationData);

        vm.synchLookups();
      });
  };

  vm.synchLookups = function synchLookups() {
    var synchedData = fleetVehiclePropertySynchronizer.getSynchronizedData(vm.vehicle);
    vm.models = synchedData.models;
    vm.years = synchedData.years;
    vm.colors = synchedData.colors;
  };

  vm.isEditable = function isEditable() {
    return fleetLocationVehicleDetailsModeService.isAddMode() || fleetLocationVehicleDetailsModeService.isEditMode();
  };

  vm.save = function save() {
    implementationStrategy.save(vm.location.id, vm.vehicle);
  };

  /* eslint-disable no-unused-vars */
  vm.onLoadImageFile = function onLoadImageFile($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
  /* eslint-enable no-unused-vars */
    vm.imageFileErrorMessage = $invalidFiles && $invalidFiles[0] ? 'File exceeds maximum size of 70KB.' : '';

    if (!$file) {
      return;
    }

    vehicleImageService.getImageData($file)
    .then(function updateImage(imageData) {
      vm.vehicle.image = imageData;
      $scope.vehicleForm.$setDirty();
    });
  };

  vm.initialize();
}

FleetLocationVehicleDetailsController.$inject = [
  '$scope',
  '$state',
  'faFleetLocationVehicleStrategyFactory',
  'faFleetLocationVehicleDetailsModeService',
  'faFleetVehiclePropertySynchronizer',
  'faVehicleImageService'
];

fleet.controller('FaFleetLocationVehicleDetailsController', FleetLocationVehicleDetailsController);
