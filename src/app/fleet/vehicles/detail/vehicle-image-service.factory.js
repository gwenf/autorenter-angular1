'use strict';

var fleet = require('./../../fleet.module');

var vehicleImageService =
  function vehicleImageService($q, $window) {
    var getImageData = function getImageData(file) {
      var deferred = $q.defer();
      var fileReader = new $window.FileReader();
      fileReader.onload = function updateImage(event) {
        deferred.resolve(event.target.result);
      };
      fileReader.readAsDataURL(file);
      return deferred.promise;
    };

    return {
      getImageData: getImageData
    };
  };

vehicleImageService.$inject = [
  '$q',
  '$window'
];

fleet
  .factory('faVehicleImageService', vehicleImageService);
