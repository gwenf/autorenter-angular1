'use strict';

function NavBarComponentController($state, $rootScope, authenticationService) {
  var vm = this;

  vm.handleTopRightButtonClick = function handleTopRightButtonClick() {
    if (!authenticationService.isUserAuthenticated()) {
      $state.go('login');
    }
  };

  vm.logOutUser = function logOutUser() {
    authenticationService.logOut();
    $rootScope.$emit('logout');
  };

  vm.isUserAuthenticated = function isUserAuthenticated() {
    return authenticationService.isUserAuthenticated();
  };

  vm.username = function username() {
    return authenticationService.getCredentials().username;
  };
}

NavBarComponentController.$inject = [
  '$state',
  '$rootScope',
  'faAuthenticationService'
];

module.exports = NavBarComponentController;
