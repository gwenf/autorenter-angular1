'use strict';

var navBar = require('./nav-bar.module');
var navBarTemplate = require('./nav-bar.component.html');
var NavBarComponentController = require('./nav-bar.component.controller');

module.exports = navBar.component('faNavBar', {
  template: navBarTemplate,
  bindings: {
    user: '='
  },
  controller: NavBarComponentController,
  controllerAs: 'vm'
});
