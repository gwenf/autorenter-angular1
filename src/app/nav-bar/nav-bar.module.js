'use strict';

var angular = require('angular');
var authentication = require('../authentication/authentication.module');

module.exports = angular.module('fa.navbar', [
  'ui.router',
  authentication.name
]);
