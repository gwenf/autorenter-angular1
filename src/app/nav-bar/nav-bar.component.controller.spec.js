'use strict';

require('./nav-bar.component');

describe('fa.navBar.NavBarComponentController > ', function describeImpl() {
  var controller;
  var rootScope;
  var fakeAuthenticationService;
  var fakeStateService;

  beforeEach(angular.mock.module('fa.navbar'));
  
  beforeEach(function beforeEach() {
    fakeAuthenticationService = {
      logOut: function logOut() {}
    };
    fakeStateService = {
      go: function go() {}
    };

    inject(function injectImpl(_$componentController_, _$rootScope_) {
      controller = _$componentController_(
        'faNavBar', {
          $state: fakeStateService,
          faAuthenticationService: fakeAuthenticationService
        }
      );
      rootScope = _$rootScope_;
    });
  });

  describe('handleTopRightButtonClick', function handleTopRightButtonClickTests() {
    var goSpy;

    beforeEach(function beforeEach() {
      goSpy = sinon.spy(fakeStateService, 'go');
    });

    it('should go to login if not authenticated', function testImpl() {
      fakeAuthenticationService.isUserAuthenticated = function isUserAuthenticated() {
        return false;
      };
      controller.handleTopRightButtonClick();
      goSpy.args.should.deep.equal([['login']]);
    });

    it('should not go to login if authenticated', function testImpl() {
      fakeAuthenticationService.isUserAuthenticated = function isUserAuthenticated() {
        return true;
      };
      controller.handleTopRightButtonClick();
      goSpy.called.should.be.false;
    });
  });
  
  describe('logOutUser', function logOutUserTests() {
    it('should log out of the authentication service', function testImpl() {
      var logOutSpy = sinon.spy(fakeAuthenticationService, 'logOut');  
      controller.logOutUser();
      expect(logOutSpy.called).to.be.true;
    });

    it('should emit logout event', function testImpl() {
      var emitSpy = sinon.spy(rootScope, '$emit');
      controller.logOutUser();
      emitSpy.args.should.deep.equal([['logout']]);
    });
  });

  describe('isUserAuthenticated', function isUserAuthenticatedTests() {
    it('should delegate to authentication service', function testImpl() {
      var dummyReturnToProveDelegation = 'foobar';
      fakeAuthenticationService.isUserAuthenticated = function isUserAuthenticated() {
        return dummyReturnToProveDelegation;
      };
      var actualResult = controller.isUserAuthenticated();
      actualResult.should.equal(dummyReturnToProveDelegation);
    });
  });

  describe('username', function usernameTests() {
    it('should return username from credentials', function testImpl() {
      var username = 'barney';
      var credentials = {
        username: username
      };
      fakeAuthenticationService.getCredentials = function getCredentials() {
        return credentials;
      };
      var actualResult = controller.username();
      actualResult.should.equal(username);
    });
  });
});
