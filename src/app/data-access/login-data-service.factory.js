'use strict';

var dataAccess = require('./data-access.module');

var loginDataService = function loginDataService($http, generalConfig, strings) {
  function login(username, password) {
    return $http({
      method: 'POST',
      url: strings.format('{apiUrl}login', {apiUrl: generalConfig.apiUrlRoot}),
      data: { 'username': username, 'password': password }
    });
  }

  return {
    login: login
  };
};

dataAccess.$inject = [
  '$http',
  'generalConfig',
  'strings'
];

dataAccess
  .factory('faLoginDataService', loginDataService);
