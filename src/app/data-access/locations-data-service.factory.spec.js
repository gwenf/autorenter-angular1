'use strict';

require('./locations-data-service.factory');

describe('fa.dataAccess.locationsDataService > ', function describeImpl() {

  var $httpBackend;
  var generalConfig;
  var strings;
  var locationsDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(
                                        _$httpBackend_,
                                        _generalConfig_,
                                        _strings_,
                                        _faLocationsDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    locationsDataService = _faLocationsDataService_;
  }));

  describe('getLocations', function getLocationsTests() {
    var url = null;
    var expectedStatus = 200;
    var expectedLocations = { foo: 'dummy data object; the actual value does not matter' };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedLocations);
    });

    it('returns location data', function testImpl() {
      locationsDataService.getLocations()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedLocations);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      locationsDataService.getLocations()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });

  describe('addLocation', function addLocationTests() {
    var url = null;
    var locationId = 'abc123';
    var expectedStatus = 201;
    var location = { id: locationId };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
          .expect('POST', url, location)
          .respond(expectedStatus);
    });

    it('returns expected status code', function testImpl() {
      locationsDataService.addLocation(location)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });

  describe('getLocation', function getLocationTests() {
    var url = null;
    var locationId = 'abc123';
    var expectedStatus = 200;
    var expectedLocation = { id: locationId };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations/{locationId}', { apiUrl: generalConfig.apiUrlRoot, locationId: locationId });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedLocation);
    });

    it('returns location data', function testImpl() {
      locationsDataService.getLocation(locationId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedLocation);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      locationsDataService.getLocation(locationId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });

  describe('updateLocation', function updateLocationTests() {
    var url = null;
    var locationId = 'abc123';
    var expectedStatus = 200;
    var location = { id: locationId };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations/{locationId}',
        { apiUrl: generalConfig.apiUrlRoot, locationId: locationId });
      $httpBackend
          .expect('PUT', url, location)
          .respond(expectedStatus);
    });

    it('returns expected status code', function testImpl() {
      locationsDataService.updateLocation(location)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });

  describe('deleteLocation', function deleteLocationTests() {
    var url = null;
    var locationId = 'abc123';
    var expectedStatus = 204;
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations/{locationId}',
        { apiUrl: generalConfig.apiUrlRoot, locationId: locationId });
      $httpBackend
          .expect('DELETE', url)
          .respond(expectedStatus);
    });

    it('returns expected status code', function testImpl() {
      locationsDataService.deleteLocation(locationId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });
});