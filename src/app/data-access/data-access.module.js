'use strict';

var angular = require('angular');
var strings = require('../imports/strings.import');

module.exports = angular.module('fa.dataAccess', [ strings.name ]);
