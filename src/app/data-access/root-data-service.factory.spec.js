'use strict';

require('./root-data-service.factory');

describe('fa.dataAccess.rootDataService > ', function describeImpl() {

  var $httpBackend;
  var generalConfig;
  var strings;
  var rootDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(
                                        _$httpBackend_,
                                        _generalConfig_,
                                        _strings_,
                                        _faRootDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    rootDataService = _faRootDataService_;
  }));

  describe('getRoot', function getRootTests() {
    var url = null;
    var expectedStatus = 200;
    var expectedRoot = 'dummy value';
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedRoot);
    });

    it('returns root data', function testImpl() {
      rootDataService.getRoot()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse).to.equal(expectedRoot);
      });

      $httpBackend.flush();
    });

  });
});