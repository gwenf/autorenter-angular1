'use strict';

require('./raise-error-data-service.factory');

describe('fa.dataAccess.raiseErrorDataService > ', function describeImpl() {

  var $httpBackend;
  var generalConfig;
  var strings;
  var raiseErrorDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(
                                        _$httpBackend_,
                                        _generalConfig_,
                                        _strings_,
                                        _faRaiseErrorDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    raiseErrorDataService = _faRaiseErrorDataService_;
  }));

  describe('getRaiseError', function getRaiseErrorTests() {
    var url = null;
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}raise-error', { apiUrl: generalConfig.apiUrlRoot });
    });

    it('returns expected error status', function testImpl() {
      var expectedStatus = 500;
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, 'dummy val');

      raiseErrorDataService.getRaiseError()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

    it('returns unexpected success status', function testImpl() {
      var expectedStatus = 200;
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, 'dummy val');

      raiseErrorDataService.getRaiseError()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });
});
