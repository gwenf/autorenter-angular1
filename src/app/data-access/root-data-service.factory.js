'use strict';

var dataAccess = require('./data-access.module');

var rootDataService = function rootDataService($http, generalConfig, strings) {

  function getRoot() {
    return $http({
      method: 'GET',
      url: strings.format('{apiUrl}', {
        apiUrl: generalConfig.apiUrlRoot
      })
    })
      .then(function setResult(response) {
        return response.data;
      });
  }

  return {
    getRoot: getRoot
  };
};

dataAccess.$inject = [
  '$http',
  'generalConfig',
  'strings'
];

dataAccess
  .factory('faRootDataService', rootDataService);
