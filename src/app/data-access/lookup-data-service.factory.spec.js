'use strict';

require('./lookup-data-service.factory');

describe('fa.dataAccess.lookupDataService > ', function describeImpl() {

  var $httpBackend;
  var generalConfig;
  var strings;
  var lookupDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(
                                        _$httpBackend_,
                                        _generalConfig_,
                                        _strings_,
                                        _faLookupDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    lookupDataService = _faLookupDataService_;
  }));

  describe('getStates', function getStatesTests() {
    var url = null;
    var expectedStatus = 200;
    var expectedLookupData = { foo: 'dummy data object; the actual value does not matter' };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}lookup-data?states', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedLookupData);
    });

    it('returns state data', function testImpl() {
      lookupDataService.getStates()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedLookupData);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      lookupDataService.getStates()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });

  describe('getVehicleLookupData', function getVehicleLookupDataTests() {
    var url = null;
    var expectedStatus = 200;
    var expectedLookupData = { foo: 'dummy data object; the actual value does not matter' };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}lookup-data?makes&models', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedLookupData);
    });

    it('returns vehicle lookup data', function testImpl() {
      lookupDataService.getVehicleLookupData()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedLookupData);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      lookupDataService.getVehicleLookupData()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });
});
