'use strict';

var dataAccess = require('./data-access.module');

require('./login-data-service.factory');
require('./locations-data-service.factory');
require('./lookup-data-service.factory');
require('./sku-data-service.factory');
require('./vehicles-data-service.factory');
require('./root-data-service.factory');
require('./raise-error-data-service.factory');

module.exports = dataAccess;
