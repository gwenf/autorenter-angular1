'use strict';

require('./sku-data-service.factory');

describe('fa.dataAccess.skuDataService > ', function describeImpl() {

  var $httpBackend;
  var generalConfig;
  var strings;
  var skuDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(
                                        _$httpBackend_, 
                                        _generalConfig_,
                                        _strings_,
                                        _faSkuDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    skuDataService = _faSkuDataService_;
  }));

  describe('getSkus', function getSkusTests() {
    var url = null;
    var expectedStatus = 200;
    var expectedSkus = { foo: 'dummy data object; the actual value does not matter' };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}skus', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedSkus);
    });

    it('returns sku data', function testImpl() {
      skuDataService.getSkus()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedSkus);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      skuDataService.getSkus()
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });
});