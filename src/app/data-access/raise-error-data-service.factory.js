'use strict';

var dataAccess = require('./data-access.module');

var raiseErrorDataService = function raiseErrorDataService($http, generalConfig, strings) {

  function getRaiseError() {
    return $http({
      method: 'GET',
      url: strings.format('{apiUrl}raise-error', {
        apiUrl: generalConfig.apiUrlRoot
      })
    })
    .then(function setResult(response) {
      return response.status;
    }).catch(function errorCallback(response) {
      return response.status;
    });
  }

  return {
    getRaiseError: getRaiseError
  };
};

dataAccess.$inject = [
  '$http',
  'generalConfig',
  'strings'
];

dataAccess
  .factory('faRaiseErrorDataService', raiseErrorDataService);
