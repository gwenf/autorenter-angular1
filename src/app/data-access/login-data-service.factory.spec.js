'use strict';

require('./login-data-service.factory');

describe('fa.dataAccess.loginDataService > ', function describeImpl() {
  var $httpBackend;
  var generalConfig;
  var strings;
  var loginDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(_$httpBackend_,
                                        _generalConfig_,
                                        _strings_,
                                        _faLoginDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    loginDataService = _faLoginDataService_;
  }));

  describe('login', function LoginTests() {
    var url = null;
    var expectedStatus = 200;
    var actualResponse = null;
    var userData = {
      'username': 'bob',
      'password': 'bobs_password'
    };
    var expectedData = {
      'id': '7cafedfb-616c-4093-bec6-b167bcd18ca8',
      'username': 'bobsmith'
    };

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}login', { apiUrl: generalConfig.apiUrlRoot });
      $httpBackend
        .expect('POST', url, userData)
        .respond(expectedStatus, expectedData);
    });

    it('returns as expected', function testImpl() {
      loginDataService.login('bob','bobs_password')
        .then(function processResponse(apiResponse) {
          actualResponse = apiResponse;
          expect(actualResponse.status).to.equal(expectedStatus);
          expect(actualResponse.data).to.eql(expectedData);
        });

      $httpBackend.flush();
    });
  });
});
