'use strict';

require('./vehicles-data-service.factory');

describe('fa.dataAccess.vehiclesDataService > ', function describeImpl() {

  var $httpBackend;
  var generalConfig;
  var strings;
  var vehiclesDataService;

  beforeEach(angular.mock.module('fa.config'));
  beforeEach(angular.mock.module('fa.dataAccess'));

  beforeEach(inject(function injectImpl(
                                        _$httpBackend_,
                                        _generalConfig_,
                                        _strings_,
                                        _faVehiclesDataService_) {
    $httpBackend = _$httpBackend_;
    generalConfig = _generalConfig_;
    strings = _strings_;
    vehiclesDataService = _faVehiclesDataService_;
  }));

  describe('getVehicles', function getVehiclesTests() {
    var url = null;
    var expectedStatus = 200;
    var locationId = '123abc';
    var expectedVehicles = { foo: 'dummy data object; the actual value does not matter' };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations/{locationId}/vehicles/',
        { apiUrl: generalConfig.apiUrlRoot, locationId: locationId });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedVehicles);
    });

    it('returns vehicle data', function testImpl() {
      vehiclesDataService.getVehicles(locationId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedVehicles);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      vehiclesDataService.getVehicles(locationId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });

  describe('addVehicleToLocation', function addVehicleTests() {
    var url = null;
    var locationId = 'abc123';
    var expectedStatus = 201;
    var vehicle = { foo: 'dummy data object; the actual value does not matter' };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}locations/{locationId}/vehicles/',
        { apiUrl: generalConfig.apiUrlRoot, locationId: locationId });
      $httpBackend
          .expect('POST', url, vehicle)
          .respond(expectedStatus);
    });

    it('returns expected status code', function testImpl() {
      vehiclesDataService.addVehicleToLocation(locationId, vehicle)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });

  describe('getVehicle', function getVehicleTests() {
    var url = null;
    var vehicleId = 'abc123';
    var expectedStatus = 200;
    var expectedVehicle = { id: vehicleId };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}vehicles/{vehicleId}',
        { apiUrl: generalConfig.apiUrlRoot, vehicleId: vehicleId });
      $httpBackend
          .expect('GET', url)
          .respond(expectedStatus, expectedVehicle);
    });

    it('returns vehicle data', function testImpl() {
      vehiclesDataService.getVehicle(vehicleId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.data).to.eql(expectedVehicle);
      });

      $httpBackend.flush();
    });

    it('returns expected status code', function testImpl() {
      vehiclesDataService.getVehicle(vehicleId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });

  });

  describe('updateVehicle', function updateVehicleTests() {
    var url = null;
    var vehicleId = 'abc123';
    var expectedStatus = 200;
    var vehicle = { id: vehicleId };
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}vehicles/{vehicleId}',
        { apiUrl: generalConfig.apiUrlRoot, vehicleId: vehicleId });
      $httpBackend
          .expect('PUT', url, vehicle)
          .respond(expectedStatus);
    });

    it('returns expected status code', function testImpl() {
      vehiclesDataService.updateVehicle(vehicle)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });

  describe('deleteVehicle', function deleteVehicleTests() {
    var url = null;
    var vehicleId = 'abc123';
    var expectedStatus = 204;
    var actualResponse = null;

    beforeEach(function beforeEachImpl() {
      url = strings.format('{apiUrl}vehicles/{vehicleId}',
        { apiUrl: generalConfig.apiUrlRoot, vehicleId: vehicleId });
      $httpBackend
          .expect('DELETE', url)
          .respond(expectedStatus);
    });

    it('returns expected status code', function testImpl() {
      vehiclesDataService.deleteVehicle(vehicleId)
      .then(function processResponse(apiResponse) {
        actualResponse = apiResponse;
        expect(actualResponse.status).to.equal(expectedStatus);
      });

      $httpBackend.flush();
    });
  });
});