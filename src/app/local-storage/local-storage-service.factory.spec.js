require('./local-storage-service.factory');

describe('fa.localstorage.localStorageService > ', function describeImpl() {
  var localStorageService;
  var windowService;
  
  beforeEach(angular.mock.module('fa.localStorage'));

  beforeEach(inject(function injectImpl(_faLocalStorageService_, _$window_) {
    localStorageService = _faLocalStorageService_;
    windowService = _$window_;
  }));

  describe('getCredentials', function getCredentialsTests() {
    it('should get the credentials from local storage', function testImpl() {
      var token = 'some fake token';
      var userId = 'some fake user id';
      var username = 'some fake user name';
      windowService.localStorage.token = token;
      windowService.localStorage.userId = userId;
      windowService.localStorage.username = username;
      var credentials = localStorageService.getCredentials();
      credentials.should.deep.equal({
        username: username,
        userId: userId,
        token: token
      });
    });
  });

  describe('getUserToken', function getUserTokenTests() {
    it('should get the correct token', function testImpl() {
      var expectedToken = 'foobar';
      windowService.localStorage.token = expectedToken;
      var token = localStorageService.getUserToken();
      expect(token).to.equal(expectedToken);
    });
  });

  describe('clearCredentials', function clearCredentialsTests() {
    it('should clear the credentials from local storage', function testImpl() {
      windowService.localStorage.token = 'some fake token';
      windowService.localStorage.userId = 'some fake user id';
      windowService.localStorage.username = 'some fake user name';
      localStorageService.clearCredentials();
      expect(windowService.localStorage.token).to.be.undefined;
      expect(windowService.localStorage.userId).to.be.undefined;
      expect(windowService.localStorage.username).to.be.undefined;
    });
  });

  describe('setCredentials', function testImpl() {
    it('should set credentials in local storage',
    function testImpl() {
      var credentials = {
        username: 'Payton Channing',
        userId: 'chicken',
        token: 'im a token'
      };
      windowService.localStorage.token = undefined;
      windowService.localStorage.userId = undefined;
      windowService.localStorage.username = undefined;
      localStorageService.setCredentials(credentials);
      expect(windowService.localStorage.userId).to.equal(credentials.userId);
      expect(windowService.localStorage.username).to.equal(credentials.username);
      expect(windowService.localStorage.token).to.equal(credentials.token);
    });
  });

});
