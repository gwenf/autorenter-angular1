'use strict';

var localStorage = require('./local-storage.module');

function localStorageService($window) {
  function getCredentials() {
    return {
      username: $window.localStorage.username,
      userId: $window.localStorage.userId,
      token: $window.localStorage.token
    };
  }

  function getUserToken() {
    return getCredentials().token;
  }

  function clearCredentials() {
    delete $window.localStorage.username;
    delete $window.localStorage.token;
    delete $window.localStorage.userId;
  }

  function setCredentials(credentials) {
    $window.localStorage.token = credentials.token;
    $window.localStorage.username = credentials.username;
    $window.localStorage.userId = credentials.userId;
  }

  return {
    getCredentials: getCredentials,
    getUserToken: getUserToken,
    clearCredentials: clearCredentials,
    setCredentials: setCredentials
  };
}

localStorageService.$inject = ['$window'];

localStorage.factory('faLocalStorageService', localStorageService);
