'use strict';

var localStorage = require('./local-storage.module');

require('./local-storage-service.factory');

module.exports = localStorage;
