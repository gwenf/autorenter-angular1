/**
 * This file is an entry point for angular tests.
 * Avoids some weird issues when using webpack + angular.
 * */
'use strict';

var sinon = require('sinon');
var chai = require('chai');
var sinonChai = require('sinon-chai');
var chaiAsPromised = require('chai-as-promised');

chai.config.includeStack = true;
chai.config.showDiff = true;

chai.use(sinonChai);
chai.use(chaiAsPromised);

global.chai = chai;
global.sinon = sinon;
global.expect = chai.expect;
global.should = chai.should();

require('angular');
require('angular-mocks');

var testsContext = require.context('../../src', true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);
