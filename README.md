[![Build Status][travis-image]][travis-url]

# AutoRenter - Angular 1

An Angular based implementation of the AutoRenter UI.

The spec is located [here](https://www.gitbook.com/book/fusion-alliance/autorenter-spec/details).

## Overview

These instructions will cover usage information for the UI.

## Prerequisites

The api must also be running for the UI to work properly. Please follow directions in the corresponding readme.

- Make sure the project is at a location with minimal file path length (this is especially important in a Windows environment!). For this project we strongly recommend `c:/aur/ui` as the project root.
- Install [Git](https://git-scm.com/downloads).
- Install [Node](https://nodejs.org/en/download/) (tested on version 6.2.2)
- Clone the Git repository to your local machine.
- Install [Chrome](https://www.google.com/chrome/). (Optional - Only needed for debugging)
- Make a copy of the `.env.example` file, and rename the copy to `.env`.

## How To

**Unless otherwise noted, all terminal commands must be issued from the project's root directory.**

### Install project libraries

```bash
npm install
```

### Run tests

```bash
npm test
```

Watch mode:
```bash
npm run test:watch
```

Debug with chrome:
```bash
npm run test:debug
```

### Start the app

To start the app with all debug logging enabled (recommended):

```bash
npm run dev
```

## Browse the App

After successfully starting the UI app, you should be able to run the application by browsing to the URL specfied by the host and port environment variables.

## Troubleshooting

### npm "Maximum call stack size exceeded"

* Try running `npm install` again. After a few tries it will finally succeed. Usually.

### Everything Is Hosed!

Sometimes you just need to completely clean your development environment before starting over from the beginning. The following commands will help you start from a "clean slate":

```bash
# Blow away the node_modules folder:
rimraf node_modules
```

## Style Guide

Please refer to the team's Angular 1 Style Guide, located [here](./style_guide.md).

## Contributing

Please read the [CONTRIBUTING](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Fusion Alliance for the initiative to create a community of open source development within our ranks.

[travis-url]: https://travis-ci.org/fusionalliance/autorenter-angular1
[travis-image]: https://travis-ci.org/fusionalliance/autorenter-angular1.svg?branch=development&style=flat-square
