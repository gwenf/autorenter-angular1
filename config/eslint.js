const webpackEsLintConfig = {
  configFile: './.eslintrc',
  failOnError: true
};

module.exports = webpackEsLintConfig;
