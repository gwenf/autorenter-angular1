const webpackEntryConfig = ({ entryJs }) => {
  return {
    app: [
      entryJs,
      'webpack/hot/dev-server'
    ],
    vendor: [
      // 3rd dependencies
      'fa-strings.js',
  
      // angular
      'angular',
      'angular-animate',
      'angular-breadcrumb',
      'angular-loading-bar',
      'angular-ui-bootstrap',
      'angular-ui-grid',
      'angular-ui-router',
      'angularjs-toaster',
      'angular-mocks',
      'angulartics'
    ]
  }
};

module.exports = webpackEntryConfig;
