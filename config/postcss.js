const postcssImport = require('postcss-import'),
      autoprefixer = require('autoprefixer'),
      webpack = require('webpack');

const buildWebpackPostCSSConfig = () => {
  return [postcssImport({
    addDependencyTo: webpack
  }), autoprefixer];
};

module.exports = buildWebpackPostCSSConfig;
