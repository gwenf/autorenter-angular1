const webpackPreloadersConfig = [
  {
    test: /\.js$/,
    loader: 'eslint',
    exclude: /node_modules/
  }
];

module.exports = webpackPreloadersConfig;
