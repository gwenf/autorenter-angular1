const path = require('path');
const buildWebpackDevServerConfig = ({ host, port, base }) => {
  return {
    contentBase: path.join(base, 'dist'),
    historyApiFallback: true,
    hot: true,
    inline: true,
    stats: {
      modules: false,
      cached: false,
      colors: true,
      chunk: false
    },
    host: host,
    port: port
  };
};

module.exports = buildWebpackDevServerConfig;
