var ExtractTextPlugin = require('extract-text-webpack-plugin');

const webpackLoadersConfig = [
  {
    test: /\.js$/,
    loader: 'ng-annotate',
    exclude: /node_modules/
  },
  {
    test: /.html$/,
    loader: 'html',
  },
  {
    test: /\.css$/,
    loader: ExtractTextPlugin.extract('style', 'css?sourceMap|postcss')
  },
  {
    test: /\.(woff|woff2|ttf|eot|svg)(\?]?.*)?$/,
    loader: 'file?name=assets/fonts/[name].[ext]?[hash]'
  },
  {
    test: /\.(png|jpg|jpeg|gif)$/,
    loader: 'url?limit=8192&name=assets/images/[name].[hash].[ext]'
  }
];

module.exports = webpackLoadersConfig;
