var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const buildWebpackPluginsConfig = ({ isProd, apiUrl, base, appName }) => {
  let additionalPlugins = [];
  if (isProd) {
    additionalPlugins.push(
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        },
        mangle: false
      }),
      new webpack.optimize.OccurenceOrderPlugin()
    );
  }
  return [
    new webpack.DefinePlugin({
      __PROD__: JSON.stringify(isProd),
      __APIURL__: JSON.stringify(apiUrl)
    }),
    new webpack.optimize.CommonsChunkPlugin('vendor', isProd ? 'vendor.[hash].js' : 'vendor.js'),
    new ExtractTextPlugin(isProd ? '[name].[hash].css' : '[name].css'),
    new HtmlWebpackPlugin( {
      template: base + '/src/index.html',
      chunks: ['app', 'vendor'],
      favicon: base + '/src/assets/img/favicon.ico',
      appName: appName
    }),
    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    })
  ].concat(additionalPlugins);
};

module.exports = buildWebpackPluginsConfig;
