const webpackOutputConfig = ({base, isProd}) => {
  return {
    path: base + '/dist',
    filename: isProd ? '[name].[hash].js' : '[name].js',
    chunkFilename: isProd ? '[name].[hash].chunk.js' : '[name].chunk.js'
  };
};

module.exports = webpackOutputConfig;
