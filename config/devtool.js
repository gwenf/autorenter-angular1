const buildWebpackDevtoolConfig = ({ isProd }) => {
  return isProd ? 'source-map' : 'eval-source-map'
};

module.exports = buildWebpackDevtoolConfig;
