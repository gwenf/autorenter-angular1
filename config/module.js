const webpackPreloadersConfig = require('./preloaders');
const webpackLoadersConfig = require('./loaders');

const webpackModuleConfig = {
  preLoaders: webpackPreloadersConfig,
  loaders: webpackLoadersConfig
};

module.exports = webpackModuleConfig;
