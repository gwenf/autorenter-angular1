require('dotenv').config();
const args = require('yargs').argv;

const buildDefaultOptions = (dir) => {
  return {
    isProd: args.prod,
    apiUrl: process.env.__APIURL__,
    base: dir,
    entryJs: dir + process.env.APP_ENTRY_FILE,
    appName: process.env.APP_NAME,
    host: process.env.HOST,
    port: process.env.PORT
  };
}

module.exports = buildDefaultOptions;
